from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Add_Friend_Form
from .models import Friends

# Create your views here.
response = {}
def index(request):    
    response['author'] = "Priscilla Tiffany" #TODO Implement yourname
    allFriends = Friends.objects.all()
    response['allFriends'] = allFriends
    response['add_friend_form'] = Add_Friend_Form
    return render(request, 'index_friend.html', response)

def add_friend(request):
    form = Add_Friend_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['url'] = request.POST['url']
        friend = Friends(name=response['name'],url=response['url'])
        friend.save()
        return HttpResponseRedirect('/friend/')
    else:
        return HttpResponseRedirect('/friend/')