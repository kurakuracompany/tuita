from django import forms

class Add_Friend_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    title_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'Masukan nama teman'
    }
    description_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'Masukan url teman'
    }

    name = forms.CharField(label='Name', required=True, max_length=27, widget=forms.TextInput(attrs=title_attrs))
    url = forms.CharField(label='URL', required=True, widget=forms.TextInput(attrs=description_attrs))