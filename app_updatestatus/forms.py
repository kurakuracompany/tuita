from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Tolong dong isi :c',
    }
    isi_attrs = {
        'type': 'text',
        'cols':70,
        'rows':4,
        'class': 'status-form-textarea',
        'placeholder':'Mikirin apaan ???'
    }

    isi = forms.CharField(label='', required=True, max_length=140, widget=forms.Textarea(attrs=isi_attrs))
    
